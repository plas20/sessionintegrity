

type 'a t = 'a list * 'a list

let empty = [],[]

let add x (l1,l2) = x::l1,l2

exception Empty

let take (l1,l2) = match l2 with
  | [] -> begin match List.rev l1 with
          | [] -> raise Empty
          | t :: q -> t,([],q) end
  | t :: q -> t,(l1,q)


let length (l1,l2) = List.length l1 + List.length l2

                  
        
