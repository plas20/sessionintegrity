
let debug = ref false
let show_configurations = ref false

module Web (Gen : Semantics.Gencc) = struct

  open Semantics
  open Gen
  open Serverast
  open Clientast

let next = ref 0
let fresh () = incr next ; "$" ^ string_of_int !next
     
  
  let init_step u y v conf =
    let sconf = Umap.find u conf.ss in
    let v',mu = Server.step_star (Server.setcookie
                                    (Uset.mem u conf.l)
                                    (Server.sety y v sconf)) in
    { ss = Umap.add u (Server.resety y mu sconf) conf.ss ;
      cc = Dynqueue.add (gencc u v', Dynqueue.empty, u) conf.cc ;
      s = Umap.add u (Input v' :: UserInit (u,y,v)
                      :: (try Umap.find u conf.s
                          with Not_found -> []))conf.s ;
      l = if v' = Login u then Uset.add u conf.l else conf.l }

  let step conf =
    if conf.cc = Dynqueue.empty then conf else begin
        let (cc,b,u_c),new_cc = Dynqueue.take conf.cc in
        match cc with
          Client.NT(p,mu) ->
           let cc',fl = Client.step mu p in
           if !debug then begin
             match cc' with
             |NT (cc',mu) -> Format.printf "Client code to execute next :@.@.->%a@.@. with memory @.%a @.@."
                              Pprint.Printclient.print_program cc' Pprint.Printclient.print_mem mu
             |_ -> ()end ;
           begin match fl with
           |Fnone -> {ss = conf.ss;
                      cc = Dynqueue.add (cc',b,u_c) new_cc;
                      s = conf.s;
                      l = conf.l}
           |Fcall (u,y,v,x,p) ->
             let sconf = Umap.find u conf.ss in
             let v',mu = Server.step_star
                           (Server.setcookie
                              (Uset.mem u conf.l)
                              (Server.sety (y)
                                 (Server.sval_of_cval v) sconf)) in
             {ss = Umap.add u (Server.resety (y) mu sconf) conf.ss;
              cc = Dynqueue.add (cc',Dynqueue.add (x,p,genv v') b,u_c) new_cc;
              s = Umap.add u (Input v'::
                                Output (u,y,
                                        Server.sval_of_cval v)::
                                (try Umap.find u conf.s
                                with Not_found -> [])) conf.s;
              l = if v' = Login u
                  then Uset.add u conf.l else conf.l}
           |Fsilent (u,y,v,x,p) ->
             let sconf = Umap.find u conf.ss in
             let v',mu = Server.step_star
                           (Server.setcookie
                              false
                              (Server.sety (y)
                                 (Server.sval_of_cval v) sconf)) in
             {ss = Umap.add u (Server.resety (y) mu sconf) conf.ss;
              cc = Dynqueue.add (cc', Dynqueue.add (x,p,genv v') b, u_c) new_cc;
              s = Umap.add u (StarInput v' :: StarOutput
                              (u,y,Server.sval_of_cval v)::
                                (try Umap.find u conf.s
                                 with Not_found -> []))conf.s;
              l = if v' = Login u
                  then Uset.add u conf.l else conf.l}
           |Fcb (x,p,v) -> {ss = conf.ss;
                            cc = Dynqueue.add (cc',Dynqueue.add (x,p,v) b,u_c) new_cc;
                            s = conf.s;
                            l = conf.l}
           |Flogin (u,p1,p2) -> begin match cc' with
                                |U mu -> let cc'' = if Uset.mem u conf.l then Client.NT(p1,mu) else Client.NT(p2,mu) in
                                         {ss = conf.ss;
                                          cc = Dynqueue.add (cc'',b,u_c) new_cc;
                                          s = conf.s;
                                          l = conf.l}
                                | _ -> failwith "Flag login with not Undefined configuration" end
           |Fyield p -> begin match cc' with
                        | U mu -> let cc'' = Client.T(Undefined,mu) in
                                  { ss = conf.ss ;
                                    cc = Dynqueue.add (cc'',Dynqueue.add (fresh (),p,Undefined) b,u_c) new_cc;
                                    s = conf.s;
                                    l = conf.l}
                        | _ -> failwith "Flag yield with not Undefined configuration" end
                                
           end
          |Client.T (_,mu) ->
            begin try let (x,p,v),b = Dynqueue.take b in
                     { ss = conf.ss;
                       cc = Dynqueue.add (Client.NT (p,Client.Mem.add x v mu),b,u_c) new_cc;
                       s = conf.s;
                       l = conf.l }
                     with Dynqueue.Empty -> {ss = conf.ss;
                                             cc = new_cc;
                                             s = conf.s;
                                             l = conf.l}end
      |Client.U _ -> failwith "no return at end of sequence" end
    
  


  module P = Pprint.Printweb(Gen)


           
  let rec run conf l max_steps steps =
    if !show_configurations
    then Format.printf "Step %d :@. @[%a@]@." steps
           P.print_conf conf ;
    if steps > max_steps ||
         (conf.cc = Dynqueue.empty && l = [])
    then () else 
      begin
        match l with
        |[] -> let conf = step conf in
               run conf l max_steps (steps + 1)
        |(n,_,_,_) :: q when steps < n -> let conf = step conf in
                                          run conf l max_steps
                                            (steps +1)
        |(_,u,y,v) :: q -> let conf = init_step u y v conf in
                           run conf q max_steps (steps + 1)
      end
end
          
