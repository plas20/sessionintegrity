

open Format
   

module Printclient = struct
  open Clientast


  let transform s =
    let rec aux i s' =
      if i = String.length s then s' else begin
          match s.[i] with
          |'\n' -> aux (i+1) (s' ^ "\\n")
          |'\t' -> aux (i+1) (s' ^ "\\t")
          |'\"' -> aux (i+1) (s' ^ "\\\"")
          |'\\' -> aux (i+1) (s' ^ "\\\\")
          |x -> aux (i+1) (s' ^ String.make 1 x)
        end in aux 0 ""
                                                         
     
  let rec print_value fmt = function
      Int n -> fprintf fmt "%i" n
    | String s -> fprintf fmt "@[\"%s\"@]@ " (transform s)
    | Bool b -> fprintf fmt "%s" (if b then "true" else "false")
    | Undefined -> fprintf fmt "Undefined"
    | Login u -> fprintf fmt "vlogin %s" u
    | Pair (v1,v2) ->
       fprintf fmt "(@[%a@],@[%a@])" print_value v1 print_value v2
    | Dummy -> fprintf fmt "Dummy"
      
      
  let rec print_expr fmt = function
      Value v -> print_value fmt v
    | Variable x -> fprintf fmt "%s" x
    | Op (o,l) ->
       begin match o,l with
       | "plus", [e1;e2] -> fprintf fmt "(@[%a@])+(@[%a@])"
                              print_expr e1 print_expr e2
       | "minus", [e] -> fprintf fmt "-(@[%a@])" print_expr e
       | "minus", [e1;e2] -> fprintf fmt "(@[%a@])-(@[%a@])"
                               print_expr e1 print_expr e2
       | "times", [e1;e2] -> fprintf fmt "@[%a@]*@[%a@]"
                               print_expr e1 print_expr e2
       | "div", [e1;e2] -> fprintf fmt "@[%a@]/@[%a@]"
                             print_expr e1 print_expr e2
       | "mod", [e1;e2] -> fprintf fmt "@[%a@] mod @[%a@]"
                           print_expr e1 print_expr e2
       | "and", [e1;e2] -> fprintf fmt "@[%a@]&&@[%a@]"
                             print_expr e1 print_expr e2
       | "or", [e1;e2] -> fprintf fmt "(@[%a@])||(@[%a@])"
                            print_expr e1 print_expr e2
       | "not", [e] -> fprintf fmt "not @[%a@]" print_expr e
       | "g", [e1;e2] -> fprintf fmt "@[%a@]>@[%a@]"
                           print_expr e1 print_expr e2
       | "geq", [e1;e2] -> fprintf fmt "@[%a@]>=@[%a@]"
                             print_expr e1 print_expr e2
       | "l", [e1;e2] -> fprintf fmt "@[%a@]<@[%a@]"
                           print_expr e1 print_expr e2
       | "leq", [e1;e2] -> fprintf fmt "@[%a@]<=@[%a@]"
                             print_expr e1 print_expr e2
       | "eq", [e1;e2] -> fprintf fmt "@[%a@]=@[%a@]"
                            print_expr e1 print_expr e2
       | "neq", [e1;e2] -> fprintf fmt "@[%a@]!=@[%a@]"
                             print_expr e1 print_expr e2
       | "concat", [e1;e2] -> fprintf fmt "@[%a@] ^ @[%a@]"
                             print_expr e1 print_expr e2
       | "pair", [e1;e2] -> fprintf fmt "(@[@[%a@],@ @[%a@]@])"
                              print_expr e1 print_expr e2
       | _, _ -> fprintf fmt "%s(%a)" o print_expr_list l end
      
  and print_expr_list fmt = function
    | [] -> ()
    | [e] -> print_expr fmt e
    | e::q -> fprintf fmt "@[%a@] ;@ %a" print_expr e print_expr_list q
            
  let rec print_program fmt = function
      Assign (x,e) -> fprintf fmt "%s :=@ @[%a@]" x print_expr e
    | Yield -> fprintf fmt "yield!"
    | Sequence (t1,t2) -> fprintf fmt "@[%a@];@ @[%a@]"
                            print_program t1 print_program t2
    | If (e,t1,t2) -> fprintf fmt "if @[%a@]@ then @[%a@]@ else @[%a@]"
                        print_expr e print_program t1 print_program t2
    | Iflogin (u,t1,t2) -> fprintf fmt "iflogin %s then @[%a@]@ else @[%a@]"
                       u print_program t1 print_program t2
    | While (e,t) -> fprintf fmt "while @[%a@]@ do (@[%a@])"
                       print_expr e print_program t
    | Skip e -> fprintf fmt "skip @[%a@]" print_expr e
    | Return e -> fprintf fmt "return @[%a@]" print_expr e
    | Call (u,y,e,x,p) ->
       fprintf fmt "call %s@ with %s :=@[%a@]@ then lambda %s.(@[%a@])"
       u y print_expr e x print_program p
    | CallSilently (u,y,e,x,p) ->
       fprintf fmt
         "callSilently %s@ with %s :=@[%a@]@ then lambda %s.(@[%a@])"
         u y print_expr e x print_program p
    | AddCallback (x,p,v) ->
     fprintf fmt "addCallback lambda %s.(@[%a@])@[%a@]" x
       print_program p print_expr v


  let print_mem fmt mu =
    Semantics.Client.Mem.iter
      (fun x v -> fprintf fmt "%s\t\t->\t@[%a@]@." x print_value v)
      mu
      
end
                   
module Printweb (Gen : Semantics.Gencc) = struct

 
  open Semantics
  open Server
  open Serverast

  let rec print_value fmt = function
      Int n -> fprintf fmt "%i" n
    | String s -> fprintf fmt "\"%s\"" s
    | Bool b -> fprintf fmt "%s" (if b then "true" else "false")
    | Undefined -> fprintf fmt "Undefined"
    | Dummy -> fprintf fmt "Dummy"
    | Login u -> fprintf fmt "vlogin %s" u
    | Vtilde _ -> fprintf fmt "~value"
    | Pair (v1,v2) ->
       fprintf fmt "(@[%a@],@[%a@])" print_value v1 print_value v2
  let print_mem fmt mu =
    Mem.iter
      (fun x v -> fprintf fmt "%s\t\t->\t@[%a@]@." x print_value v)
      mu
    

  let print_event fmt = function
    | UserInit (u,y,v) -> fprintf fmt "UserInit to %s@ with %s := @[%a@]"
                            u y print_value v
    | Output (u,y,v) -> fprintf fmt "Output to %s@ with %s := @[%a@]"
                              u y print_value v
    | Input v -> fprintf fmt "Input from server, value @[%a@]"
                   print_value v
    | StarOutput (u,y,v) ->
       fprintf fmt
         "Starred Output to %s@ with %s := @[%a@]" u y print_value v
    | StarInput v -> fprintf fmt
                       "Starred Input from server, value @[%a@]"
                       print_value v
    
  let print_conf fmt conf =
    Umap.iter (fun u c -> fprintf fmt "%s memory:@.@[%a@]@."
                           u print_mem (mem_of_conf c)) conf.ss ;
    fprintf fmt "Number of clients : %d@." (Dynqueue.length conf.cc) ;
    let rec aux fmt = function
      | [] -> ()
      | [e] -> print_event fmt e
      | e::q -> fprintf fmt "@[%a@] ;@ %a" print_event e aux q in
    Umap.iter (fun u l -> fprintf fmt "%s events:[@[%a@]@." u aux l)
      conf.s;
    let aux2 fmt () = Uset.iter (fun u -> fprintf fmt "%s;@ " u)
                        conf.l in
    fprintf fmt "Login history contains %a@." aux2 ()
    
end
                                        
