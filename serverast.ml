type url = string
type ident = string
           
type value =
    Int of int
  | String of string
  | Bool of bool
  | Pair of value * value
  | Login of url
  | Vtilde of Clientast.program
  | Undefined
  | Dummy
  type doll =
    Dvalue of value
  | Dvariable of ident
  | Dop of string * doll list
  | Dreference of ident
  type t =
    Tassign of ident * doll
  | Tsequence of t * t
  | Tif of doll * t * t
  | Tiflogin of url * t * t
  | Twhile of doll * t
  | Tskip of doll
  | Treturn of doll
  | Tcall of url * ident * doll * ident * t
  type expression =
    Value of value
  | Variable of ident
  | Op of string * expression list
  | Tilde of t
  type program =
    Assign of ident * expression
  | Sequence of program * program
  | If of expression * program * program
  | While of expression * program
  | Skip of expression
  | Return of expression


                  
                      
