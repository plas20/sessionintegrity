


module C = Clientast
         
let rename s = "#" ^ s
let rec rename_exp = function
  |C.Value v -> C.Value v
  |C.Variable s -> C.Variable (rename s)
  |C.Op (o,l) -> C.Op (o,List.map rename_exp l)
         
let next = ref 0
let fresh () = incr next ; "$" ^ string_of_int !next
                      
let rec fu u freshq = function
  | C.Assign (x,e) -> C.Assign (rename x,rename_exp e),freshq
  | C.Return e -> C.Sequence (C.Assign ("$conts",
                                        C.Op ("minus",[C.Variable "$conts";
                                                      C.Value (C.Int 1)])),
                              C.Return (rename_exp e)),freshq
  | C.Skip s -> C.Skip (rename_exp s),freshq
  | C.Sequence (c1,c2) -> let c1',freshq = fu u freshq c1 in
                          let c2',freshq = fu u freshq c2 in
                          C.Sequence (c1',c2'),freshq
  | C.If (e,c1,c2) -> let c1',freshq = fu u freshq c1 in
                      let c2',freshq = fu u freshq c2 in
                      C.If (rename_exp e,c1',c2'), freshq
  | C.While (e,c) -> let c',freshq = fu u freshq c in
                     C.While (rename_exp e, c'), freshq
  | C.Call (v,y,e,x,c) when v = u ->
     let z = fresh () in
     let freshq = Dynqueue.add z freshq in
     let c',freshq = fu u freshq c in
     C.Sequence(
         C.Assign ("$conts", C.Op ("plus", [C.Variable "$conts";
                                            C.Value (C.Int 1)])),
         C.Call (u,
                 y,
                 rename_exp e,
                 rename x,
                 C.Sequence(C.Assign (z,C.Op( "add" , [C.Variable (rename x) ;
                                                       C.Variable z])),
                            c'))), freshq
  | C.Call (v,y,e,x,c) ->
     let c',freshq = fu u freshq c in
     C.Sequence (C.Assign ("$conts", C.Op ("plus" , [C.Variable "$conts";
                                                     C.Value (C.Int 1)])),
                 C.AddCallback(rename x,
                               c',
                               C.Value Dummy))
               ,freshq 
  | _ -> failwith "recompiling already compiled code"

let rec fbot u freshq = function
  | C.Assign _ | C.Skip _ | C.Return _ as c -> c,freshq
  | C.Sequence (c1,c2) -> let c1',freshq = fbot u freshq c1 in
                          let c2',freshq = fbot u freshq c2 in
                          C.Sequence (c1', c2'),freshq
  | C.If (e,c1,c2) -> let c1',freshq = fbot u freshq c1 in
                      let c2',freshq = fbot u freshq c2 in
                      C.If (e, c1', c2'), freshq
  | C.While (e,c) -> let c',freshq = fbot u freshq c in C.While (e, c'),freshq
  | C.Call (v,y,e,x,c) when u = v ->
     let zqueue,freshq = Dynqueue.take freshq in
     let z = "$$$" in
     let c',freshq = fbot u freshq c in 
     C.Sequence (
         C.Sequence (
             C.Sequence ( C.Assign ("$aux$",C.Op( "take" ,
                                                  [ C.Variable zqueue ])),
                          C.Assign (z, C.Op ( "fst" , [C.Variable "$aux$"]))),
             C.Assign (zqueue, C.Op ("snd", [C.Variable "$aux$"]))),
         C.AddCallback (x,c',C.Variable z)
       ), freshq
  | C.Call (v,y,e,x,c) -> let c',freshq = fbot u freshq c in
                          C.CallSilently (v,y,e,x,c'), freshq 
  | _ -> failwith "recompiling already compiled code"

module Cl = Semantics.Client

let show_compiled = ref false

let rec separate_return = function
  | C.Sequence (c, C.Return v) -> c,v
  | C.Sequence (c1, c2) -> let c,v = separate_return c2 in
                           C.Sequence (c1,c) , v
  | _ -> failwith "no return at end of sequence, so failed to separate"
                  
let compile u c0 =
  let c,v = separate_return c0 in
  let c2,freshq = fu u Dynqueue.empty c in
  let c4',freshq = fbot u freshq c0 in
  let c1 = C.Assign ("$conts", C.Value (C.Int 1)) in
  let c3 = C.Assign ("$conts", C.Op ("plus", [C.Variable "$conts";
                                              C.Value (C.Int 1)])) in
  let c4 = C.AddCallback ("?",
                          C.Sequence (
                              C.While (C.Op ("g", [C.Variable "$conts";
                                                   C.Value (C.Int 1)]),
                                       C.Yield),
                              c4'),
                          C.Value C.Dummy) in
  let c5 = C.Assign ("$conts", C.Op ("minus", [C.Variable "$conts";
                                               C.Value (C.Int 1)])) in
  let c6 = C.Return v in
  let c'' = C.Sequence (
                c1,
                C.Sequence (
                    c2,
                    C.Sequence (
                        c3,
                        C.Sequence (
                            c4,
                            C.Sequence (
                                c5,c6))))) in
  assert (freshq = Dynqueue.empty) ;
  if !show_compiled
  then
    Format.printf
      "Client configuration @.@. %a @.@. was compiled into @.@. %a @.@."
      Pprint.Printclient.print_program c
      Pprint.Printclient.print_program c'' ;
  c''
          
let gencc u = function
  |Serverast.Vtilde t -> Cl.NT (compile u t, Cl.Mem.empty)
  |v -> Cl.T (Semantics.Server.cval_of_sval v,Cl.Mem.empty)
