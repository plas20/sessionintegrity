
module MenhirBasics = struct
  
  exception Error
  
  type token = 
    | WITH
    | WHILE
    | VLOGIN
    | USER
    | UNDEFINED
    | TRUE
    | TIMES
    | TIME
    | TILDE
    | THEN
    | STRING of (
# 28 "parser.mly"
       (string)
# 21 "parser.ml"
  )
    | STEPS
    | SND
    | SKIP
    | SERVER
    | SEMICOLON
    | RP
    | RETURN
    | RB
    | POINT
    | PLUS
    | PERFORM
    | OR
    | NOT
    | NEQ
    | MOD
    | MINUS
    | MEMORY
    | LP
    | LEQ
    | LB
    | LAMBDA
    | L
    | INT of (
# 27 "parser.mly"
       (int)
# 48 "parser.ml"
  )
    | INPUTS
    | IF
    | IDENT of (
# 26 "parser.mly"
       (string)
# 55 "parser.ml"
  )
    | HAT
    | GEQ
    | G
    | FST
    | FALSE
    | EQ
    | EOF
    | ELSE
    | DOLLAR
    | DO
    | DIV
    | DEF
    | COMMA
    | COLON
    | CALL
    | AT
    | ARROW
    | AND
  
end

include MenhirBasics

let _eRR =
  MenhirBasics.Error

type _menhir_env = {
  _menhir_lexer: Lexing.lexbuf -> token;
  _menhir_lexbuf: Lexing.lexbuf;
  _menhir_token: token;
  mutable _menhir_error: bool
}

and _menhir_state = 
  | MenhirState224
  | MenhirState217
  | MenhirState209
  | MenhirState204
  | MenhirState202
  | MenhirState199
  | MenhirState197
  | MenhirState187
  | MenhirState184
  | MenhirState181
  | MenhirState179
  | MenhirState177
  | MenhirState176
  | MenhirState174
  | MenhirState172
  | MenhirState171
  | MenhirState164
  | MenhirState160
  | MenhirState151
  | MenhirState149
  | MenhirState147
  | MenhirState145
  | MenhirState143
  | MenhirState141
  | MenhirState139
  | MenhirState137
  | MenhirState135
  | MenhirState133
  | MenhirState131
  | MenhirState129
  | MenhirState127
  | MenhirState125
  | MenhirState123
  | MenhirState118
  | MenhirState117
  | MenhirState115
  | MenhirState109
  | MenhirState106
  | MenhirState103
  | MenhirState98
  | MenhirState92
  | MenhirState90
  | MenhirState88
  | MenhirState87
  | MenhirState85
  | MenhirState83
  | MenhirState82
  | MenhirState75
  | MenhirState71
  | MenhirState65
  | MenhirState63
  | MenhirState61
  | MenhirState59
  | MenhirState57
  | MenhirState55
  | MenhirState53
  | MenhirState51
  | MenhirState49
  | MenhirState47
  | MenhirState45
  | MenhirState43
  | MenhirState41
  | MenhirState39
  | MenhirState37
  | MenhirState27
  | MenhirState26
  | MenhirState23
  | MenhirState22
  | MenhirState21
  | MenhirState20
  | MenhirState19
  | MenhirState18
  | MenhirState17
  | MenhirState16
  | MenhirState15
  | MenhirState14
  | MenhirState13
  | MenhirState4
  | MenhirState3
  | MenhirState0

# 1 "parser.mly"
  
    open Serverast
    open Semantics

    let mem_of_list l =
      List.fold_left (fun mu (x,v) ->
		       Server.Mem.add x v mu) Server.Mem.empty l 
       
    let ss_of_list l =
      List.fold_left (fun ss (u,p,l) ->
		       Umap.add u (Server.NT (p,
					      mem_of_list l)) ss)
		     Umap.empty l
    

    let rec pair_of_list = function
	[] -> Value Undefined
      | t :: q -> Op ("pair", [t; pair_of_list q])

    let rec dpair_of_list = function
	[] -> Dvalue Undefined
      | t :: q -> Dop ("pair", [t; dpair_of_list q])
       
	 
# 197 "parser.ml"

let rec _menhir_goto_option_COLON_ : _menhir_env -> 'ttv_tail -> (unit option) -> 'ttv_return =
  fun _menhir_env _menhir_stack _v ->
    let _menhir_stack = (_menhir_stack, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        _menhir_run198 _menhir_env (Obj.magic _menhir_stack) MenhirState197 _v
    | SERVER | WITH ->
        _menhir_reduce31 _menhir_env (Obj.magic _menhir_stack) MenhirState197
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState197

and _menhir_goto_t : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.t) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState106 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (t1 : (Serverast.t))), _, (t2 : (Serverast.t))) = _menhir_stack in
        let _2 = () in
        let _v : (Serverast.t) = 
# 130 "parser.mly"
                              (Tsequence (t1,t2))
# 227 "parser.ml"
         in
        _menhir_goto_t _menhir_env _menhir_stack _menhir_s _v
    | MenhirState90 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | ELSE ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CALL ->
                _menhir_run94 _menhir_env (Obj.magic _menhir_stack) MenhirState109
            | IDENT _v ->
                _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState109 _v
            | IF ->
                _menhir_run88 _menhir_env (Obj.magic _menhir_stack) MenhirState109
            | LP ->
                _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState109
            | RETURN ->
                _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState109
            | SKIP ->
                _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState109
            | WHILE ->
                _menhir_run18 _menhir_env (Obj.magic _menhir_stack) MenhirState109
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState109)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState87 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (t : (Serverast.t))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Serverast.t) = 
# 134 "parser.mly"
                (t)
# 279 "parser.ml"
             in
            _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_loption_separated_nonempty_list_SEMICOLON_doll__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.doll list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | RB ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (xs : (Serverast.doll list))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (Serverast.doll) = let l = 
# 232 "<standard.mly>"
    ( xs )
# 308 "parser.ml"
         in
        
# 150 "parser.mly"
                                             (dpair_of_list l)
# 313 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_loption_separated_nonempty_list_COMMA_doll__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.doll list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | RP ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (f : (
# 26 "parser.mly"
       (string)
# 337 "parser.ml"
        ))), _, (xs : (Serverast.doll list))) = _menhir_stack in
        let _4 = () in
        let _2 = () in
        let _v : (Serverast.doll) = let l = 
# 232 "<standard.mly>"
    ( xs )
# 344 "parser.ml"
         in
        
# 155 "parser.mly"
                                                   (Dop (f,l))
# 349 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_separated_nonempty_list_SEMICOLON_expr_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.expression list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState115 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Serverast.expression list)) = _v in
        let _v : (Serverast.expression list) = 
# 144 "<standard.mly>"
    ( x )
# 369 "parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMICOLON_expr__ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState160 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Serverast.expression list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Serverast.expression))) = _menhir_stack in
        let _2 = () in
        let _v : (Serverast.expression list) = 
# 243 "<standard.mly>"
    ( x :: xs )
# 381 "parser.ml"
         in
        _menhir_goto_separated_nonempty_list_SEMICOLON_expr_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_list_arrow_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((Semantics.Server.Mem.key * Serverast.value) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState197 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (l : ((Semantics.Server.Mem.key * Serverast.value) list)) = _v in
        let (((((_menhir_stack, _menhir_s), (u : (
# 26 "parser.mly"
       (string)
# 397 "parser.ml"
        ))), _, (p : (Serverast.program))), (_5 : (unit option))), (_7 : (unit option))) = _menhir_stack in
        let _6 = () in
        let _3 = () in
        let _1 = () in
        let _v : (Semantics.Umap.key * Serverast.program *
  (Semantics.Server.Mem.key * Serverast.value) list) = 
# 67 "parser.mly"
                                                                        ((u,p,l))
# 406 "parser.ml"
         in
        let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SERVER ->
            _menhir_run1 _menhir_env (Obj.magic _menhir_stack) MenhirState204
        | WITH ->
            _menhir_reduce35 _menhir_env (Obj.magic _menhir_stack) MenhirState204
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState204)
    | MenhirState202 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : ((Semantics.Server.Mem.key * Serverast.value) list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Semantics.Server.Mem.key * Serverast.value))) = _menhir_stack in
        let _v : ((Semantics.Server.Mem.key * Serverast.value) list) = 
# 213 "<standard.mly>"
    ( x :: xs )
# 429 "parser.ml"
         in
        _menhir_goto_list_arrow_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_separated_nonempty_list_SEMICOLON_doll_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.doll list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState23 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Serverast.doll list)) = _v in
        let _v : (Serverast.doll list) = 
# 144 "<standard.mly>"
    ( x )
# 445 "parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMICOLON_doll__ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState71 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Serverast.doll list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Serverast.doll))) = _menhir_stack in
        let _2 = () in
        let _v : (Serverast.doll list) = 
# 243 "<standard.mly>"
    ( x :: xs )
# 457 "parser.ml"
         in
        _menhir_goto_separated_nonempty_list_SEMICOLON_doll_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_separated_nonempty_list_COMMA_doll_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.doll list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState26 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Serverast.doll list)) = _v in
        let _v : (Serverast.doll list) = 
# 144 "<standard.mly>"
    ( x )
# 473 "parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_COMMA_doll__ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState65 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Serverast.doll list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Serverast.doll))) = _menhir_stack in
        let _2 = () in
        let _v : (Serverast.doll list) = 
# 243 "<standard.mly>"
    ( x :: xs )
# 485 "parser.ml"
         in
        _menhir_goto_separated_nonempty_list_COMMA_doll_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run37 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState37 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState37 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState37 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState37
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState37

and _menhir_run39 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState39 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState39 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState39 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState39
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState39

and _menhir_run45 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState45 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState45 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState45 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState45
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState45

and _menhir_run47 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState47 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState47 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState47 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState47
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState47

and _menhir_run41 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState41 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState41 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState41 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState41
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState41

and _menhir_run49 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState49 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState49 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState49 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState49
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState49

and _menhir_run51 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState51 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState51 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState51 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState51
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState51

and _menhir_run53 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState53 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState53 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState53 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState53
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState53

and _menhir_run63 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState63 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState63 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState63 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState63
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState63

and _menhir_run55 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState55 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState55 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState55 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState55
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState55

and _menhir_run57 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState57 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState57 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState57 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState57
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState57

and _menhir_run59 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState59 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState59
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState59

and _menhir_run43 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState43 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState43 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState43 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState43
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState43

and _menhir_run61 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState61 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState61 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState61 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState61
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState61

and _menhir_goto_option_WITH_ : _menhir_env -> 'ttv_tail -> (unit option) -> 'ttv_return =
  fun _menhir_env _menhir_stack _v ->
    let _menhir_stack = (_menhir_stack, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | MEMORY ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let x = () in
            let _v : (unit option) = 
# 116 "<standard.mly>"
    ( Some x )
# 1043 "parser.ml"
             in
            _menhir_goto_option_COLON_ _menhir_env _menhir_stack _v
        | IDENT _ | SERVER | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _v : (unit option) = 
# 114 "<standard.mly>"
    ( None )
# 1051 "parser.ml"
             in
            _menhir_goto_option_COLON_ _menhir_env _menhir_stack _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, _), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_unt : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.t) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState103 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((((((_menhir_stack, _menhir_s), (u : (
# 26 "parser.mly"
       (string)
# 1077 "parser.ml"
        ))), (y : (
# 26 "parser.mly"
       (string)
# 1081 "parser.ml"
        ))), _, (e : (Serverast.doll))), (x : (
# 26 "parser.mly"
       (string)
# 1085 "parser.ml"
        ))), _, (t : (Serverast.t))) = _menhir_stack in
        let _10 = () in
        let _8 = () in
        let _7 = () in
        let _5 = () in
        let _3 = () in
        let _1 = () in
        let _v : (Serverast.t) = 
# 141 "parser.mly"
              (Tcall (u,y,e,x,t))
# 1096 "parser.ml"
         in
        _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
    | MenhirState87 | MenhirState106 | MenhirState90 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SEMICOLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CALL ->
                _menhir_run94 _menhir_env (Obj.magic _menhir_stack) MenhirState106
            | IDENT _v ->
                _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState106 _v
            | IF ->
                _menhir_run88 _menhir_env (Obj.magic _menhir_stack) MenhirState106
            | LP ->
                _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState106
            | RETURN ->
                _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState106
            | SKIP ->
                _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState106
            | WHILE ->
                _menhir_run18 _menhir_env (Obj.magic _menhir_stack) MenhirState106
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState106)
        | ELSE | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (t : (Serverast.t))) = _menhir_stack in
            let _v : (Serverast.t) = 
# 129 "parser.mly"
            (t)
# 1133 "parser.ml"
             in
            _menhir_goto_t _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState109 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))), _, (t1 : (Serverast.t))), _, (t2 : (Serverast.t))) = _menhir_stack in
        let _5 = () in
        let _3 = () in
        let _1 = () in
        let _v : (Serverast.t) = 
# 135 "parser.mly"
                                          (Tif (e,t1,t2))
# 1152 "parser.ml"
         in
        _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
    | MenhirState82 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))), _, (t : (Serverast.t))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (Serverast.t) = 
# 136 "parser.mly"
                              (Twhile (e,t))
# 1164 "parser.ml"
         in
        _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
    | MenhirState17 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (t : (Serverast.t))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.expression) = 
# 87 "parser.mly"
                  (Tilde t)
# 1175 "parser.ml"
         in
        _menhir_goto_expr _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run19 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState19 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState19 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState19 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState19
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState19

and _menhir_run20 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState20 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState20 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState20 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState20
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState20

and _menhir_run21 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState21 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState21 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState21 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState21
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState21

and _menhir_run22 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState22 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState22 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState22 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState22
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState22

and _menhir_run23 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState23 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState23 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState23 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState23
    | RB ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_s = MenhirState23 in
        let _v : (Serverast.doll list) = 
# 142 "<standard.mly>"
    ( [] )
# 1377 "parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMICOLON_doll__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState23

and _menhir_run25 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 26 "parser.mly"
       (string)
# 1388 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LP ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DOLLAR ->
            _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | FALSE ->
            _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | FST ->
            _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | IDENT _v ->
            _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState26 _v
        | INT _v ->
            _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState26 _v
        | LB ->
            _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | LP ->
            _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | MINUS ->
            _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | NOT ->
            _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | SND ->
            _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | STRING _v ->
            _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState26 _v
        | TRUE ->
            _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | UNDEFINED ->
            _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | VLOGIN ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState26
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState26 in
            let _v : (Serverast.doll list) = 
# 142 "<standard.mly>"
    ( [] )
# 1434 "parser.ml"
             in
            _menhir_goto_loption_separated_nonempty_list_COMMA_doll__ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState26)
    | AND | COMMA | DIV | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | MOD | NEQ | OR | PLUS | RB | RP | SEMICOLON | THEN | TIMES | WITH ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, (x : (
# 26 "parser.mly"
       (string)
# 1446 "parser.ml"
        ))) = _menhir_stack in
        let _v : (Serverast.doll) = 
# 144 "parser.mly"
              (Dvariable x)
# 1451 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run27 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState27 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState27 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState27 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState27
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState27

and _menhir_run29 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (
# 26 "parser.mly"
       (string)
# 1513 "parser.ml"
        )) = _v in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.doll) = 
# 154 "parser.mly"
                     (Dreference x)
# 1520 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_expr : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState160 | MenhirState115 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SEMICOLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FALSE ->
                _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | FST ->
                _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | IDENT _v ->
                _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState160 _v
            | INT _v ->
                _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState160 _v
            | LB ->
                _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | LP ->
                _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | MINUS ->
                _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | NOT ->
                _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | SND ->
                _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | STRING _v ->
                _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState160 _v
            | TILDE ->
                _menhir_run17 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | TRUE ->
                _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | UNDEFINED ->
                _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | VLOGIN ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState160
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState160)
        | RB ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (Serverast.expression))) = _menhir_stack in
            let _v : (Serverast.expression list) = 
# 241 "<standard.mly>"
    ( [ x ] )
# 1582 "parser.ml"
             in
            _menhir_goto_separated_nonempty_list_SEMICOLON_expr_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState16 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FALSE ->
                _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | FST ->
                _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | IDENT _v ->
                _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState164 _v
            | INT _v ->
                _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState164 _v
            | LB ->
                _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | LP ->
                _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | MINUS ->
                _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | NOT ->
                _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | SND ->
                _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | STRING _v ->
                _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState164 _v
            | TILDE ->
                _menhir_run17 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | TRUE ->
                _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | UNDEFINED ->
                _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | VLOGIN ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState164
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState164)
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Serverast.expression) = 
# 103 "parser.mly"
                   (e)
# 1643 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState164 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s), _, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Serverast.expression) = 
# 101 "parser.mly"
                                    (Op ("pair",[e1;e2]))
# 1668 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState174 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.program) = 
# 83 "parser.mly"
                    (Return e)
# 1685 "parser.ml"
         in
        _menhir_goto_unprog _menhir_env _menhir_stack _menhir_s _v
    | MenhirState181 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (
# 26 "parser.mly"
       (string)
# 1694 "parser.ml"
        ))), _, (e : (Serverast.expression))) = _menhir_stack in
        let _2 = () in
        let _v : (Serverast.program) = 
# 78 "parser.mly"
                           (Assign (x,e))
# 1700 "parser.ml"
         in
        _menhir_goto_unprog _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_separated_nonempty_list_COMMA_ntexpr_ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.expression list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState117 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (x : (Serverast.expression list)) = _v in
        let _v : (Serverast.expression list) = 
# 144 "<standard.mly>"
    ( x )
# 1716 "parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_COMMA_ntexpr__ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState151 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (xs : (Serverast.expression list)) = _v in
        let (_menhir_stack, _menhir_s, (x : (Serverast.expression))) = _menhir_stack in
        let _2 = () in
        let _v : (Serverast.expression list) = 
# 243 "<standard.mly>"
    ( x :: xs )
# 1728 "parser.ml"
         in
        _menhir_goto_separated_nonempty_list_COMMA_ntexpr_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run123 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState123 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState123 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState123 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState123
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState123

and _menhir_run125 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState125 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState125 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState125 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState125
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState125

and _menhir_run131 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState131 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState131 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState131 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState131
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState131

and _menhir_run133 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState133 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState133 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState133 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState133
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState133

and _menhir_run127 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState127 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState127 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState127 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState127
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState127

and _menhir_run135 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState135 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState135 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState135 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState135
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState135

and _menhir_run137 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState137 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState137 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState137 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState137
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState137

and _menhir_run139 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState139 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState139 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState139 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState139
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState139

and _menhir_run149 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState149 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState149 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState149 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState149
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState149

and _menhir_run141 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState141 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState141 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState141 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState141
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState141

and _menhir_run143 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState143 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState143 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState143 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState143
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState143

and _menhir_run145 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState145 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState145 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState145 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState145
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState145

and _menhir_run129 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState129 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState129 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState129 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState129
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState129

and _menhir_run147 : _menhir_env -> 'ttv_tail * _menhir_state * (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState147 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState147 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState147 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState147
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState147

and _menhir_reduce31 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : ((Semantics.Server.Mem.key * Serverast.value) list) = 
# 211 "<standard.mly>"
    ( [] )
# 2243 "parser.ml"
     in
    _menhir_goto_list_arrow_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run198 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 26 "parser.mly"
       (string)
# 2250 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | ARROW ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | FALSE ->
            _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState199
        | INT _v ->
            _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState199 _v
        | STRING _v ->
            _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState199 _v
        | TRUE ->
            _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState199
        | UNDEFINED ->
            _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState199
        | VLOGIN ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState199
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState199)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_doll : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.doll) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState27 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.doll) = 
# 152 "parser.mly"
                 (Dop ("fst", [e]))
# 2297 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | MenhirState65 | MenhirState26 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | DOLLAR ->
                _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | FALSE ->
                _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | FST ->
                _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | IDENT _v ->
                _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
            | INT _v ->
                _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
            | LB ->
                _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | LP ->
                _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | MINUS ->
                _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | NOT ->
                _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | SND ->
                _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | STRING _v ->
                _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState65 _v
            | TRUE ->
                _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | UNDEFINED ->
                _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | VLOGIN ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState65
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState65)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (Serverast.doll))) = _menhir_stack in
            let _v : (Serverast.doll list) = 
# 241 "<standard.mly>"
    ( [ x ] )
# 2376 "parser.ml"
             in
            _menhir_goto_separated_nonempty_list_COMMA_doll_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState37 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.doll) = let o = 
# 115 "parser.mly"
          ("times")
# 2393 "parser.ml"
         in
        
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2398 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | MenhirState39 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | NEQ | OR | PLUS | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 114 "parser.mly"
         ("plus")
# 2419 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2424 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState41 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.doll) = let o = 
# 120 "parser.mly"
        ("mod")
# 2441 "parser.ml"
         in
        
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2446 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | MenhirState43 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.doll) = let o = 
# 119 "parser.mly"
        ("div")
# 2457 "parser.ml"
         in
        
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2462 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | MenhirState45 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | DO | ELSE | HAT | MEMORY | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 117 "parser.mly"
       ("or")
# 2501 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2506 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState47 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | HAT | MEMORY | NEQ | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 121 "parser.mly"
        ("neq")
# 2545 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2550 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState49 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | NEQ | OR | PLUS | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 113 "parser.mly"
          ("minus")
# 2577 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2582 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState51 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 122 "parser.mly"
        ("leq")
# 2613 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2618 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState53 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 123 "parser.mly"
      ("l")
# 2649 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2654 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState55 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 124 "parser.mly"
        ("geq")
# 2685 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2690 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState57 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 125 "parser.mly"
      ("g")
# 2721 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2726 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState59 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | HAT | MEMORY | NEQ | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 118 "parser.mly"
       ("eq")
# 2765 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2770 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState61 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | HAT | MEMORY | OR | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 116 "parser.mly"
        ("and")
# 2813 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2818 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState63 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | DO | ELSE | HAT | MEMORY | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = let o = 
# 112 "parser.mly"
        ("concat")
# 2865 "parser.ml"
             in
            
# 151 "parser.mly"
                               (Dop (o, [e1;e2]))
# 2870 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState71 | MenhirState23 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | SEMICOLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | DOLLAR ->
                _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | FALSE ->
                _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | FST ->
                _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | IDENT _v ->
                _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState71 _v
            | INT _v ->
                _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState71 _v
            | LB ->
                _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | LP ->
                _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | MINUS ->
                _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | NOT ->
                _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | SND ->
                _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | STRING _v ->
                _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState71 _v
            | TRUE ->
                _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | UNDEFINED ->
                _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | VLOGIN ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState71
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState71)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | RB ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (Serverast.doll))) = _menhir_stack in
            let _v : (Serverast.doll list) = 
# 241 "<standard.mly>"
    ( [ x ] )
# 2955 "parser.ml"
             in
            _menhir_goto_separated_nonempty_list_SEMICOLON_doll_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState22 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | DOLLAR ->
                _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | FALSE ->
                _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | FST ->
                _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | IDENT _v ->
                _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState75 _v
            | INT _v ->
                _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState75 _v
            | LB ->
                _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | LP ->
                _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | MINUS ->
                _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | NOT ->
                _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | SND ->
                _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | STRING _v ->
                _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState75 _v
            | TRUE ->
                _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | UNDEFINED ->
                _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | VLOGIN ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState75
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState75)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Serverast.doll) = 
# 147 "parser.mly"
                   (e)
# 3042 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState75 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, _menhir_s), _, (e1 : (Serverast.doll))), _, (e2 : (Serverast.doll))) = _menhir_stack in
            let _5 = () in
            let _3 = () in
            let _1 = () in
            let _v : (Serverast.doll) = 
# 146 "parser.mly"
                                    (Dop ("pair",[e1;e2]))
# 3095 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState21 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | AND | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | NEQ | OR | PLUS | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.doll) = 
# 148 "parser.mly"
                   (Dop ("minus", [e]))
# 3124 "parser.ml"
             in
            _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState20 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.doll) = 
# 149 "parser.mly"
                 (Dop ("not", [e]))
# 3141 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | MenhirState19 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.doll) = 
# 153 "parser.mly"
                 (Dop ("snd", [e]))
# 3152 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | MenhirState18 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | DO ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CALL ->
                _menhir_run94 _menhir_env (Obj.magic _menhir_stack) MenhirState82
            | IDENT _v ->
                _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState82 _v
            | IF ->
                _menhir_run88 _menhir_env (Obj.magic _menhir_stack) MenhirState82
            | LP ->
                _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState82
            | RETURN ->
                _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState82
            | SKIP ->
                _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState82
            | WHILE ->
                _menhir_run18 _menhir_env (Obj.magic _menhir_stack) MenhirState82
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState82)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState83 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | ELSE | MEMORY | RB | RP | SEMICOLON | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (s : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.t) = 
# 137 "parser.mly"
                  (Tskip s)
# 3257 "parser.ml"
             in
            _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState85 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | ELSE | MEMORY | RB | RP | SEMICOLON | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Serverast.doll))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.t) = 
# 139 "parser.mly"
                    (Treturn e)
# 3306 "parser.ml"
             in
            _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState88 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CALL ->
                _menhir_run94 _menhir_env (Obj.magic _menhir_stack) MenhirState90
            | IDENT _v ->
                _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState90 _v
            | IF ->
                _menhir_run88 _menhir_env (Obj.magic _menhir_stack) MenhirState90
            | LP ->
                _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState90
            | RETURN ->
                _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState90
            | SKIP ->
                _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState90
            | WHILE ->
                _menhir_run18 _menhir_env (Obj.magic _menhir_stack) MenhirState90
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState90)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState92 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | ELSE | MEMORY | RB | RP | SEMICOLON | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (x : (
# 26 "parser.mly"
       (string)
# 3415 "parser.ml"
            ))), _, (e : (Serverast.doll))) = _menhir_stack in
            let _2 = () in
            let _v : (Serverast.t) = 
# 133 "parser.mly"
                           (Tassign (x,e))
# 3421 "parser.ml"
             in
            _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState98 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run61 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run43 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run59 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run57 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run55 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run63 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run53 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run51 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run49 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run41 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run47 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run45 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run39 _menhir_env (Obj.magic _menhir_stack)
        | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | LAMBDA ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | IDENT _v ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_stack = (_menhir_stack, _v) in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | POINT ->
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let _menhir_env = _menhir_discard _menhir_env in
                        let _tok = _menhir_env._menhir_token in
                        (match _tok with
                        | CALL ->
                            _menhir_run94 _menhir_env (Obj.magic _menhir_stack) MenhirState103
                        | IDENT _v ->
                            _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState103 _v
                        | IF ->
                            _menhir_run88 _menhir_env (Obj.magic _menhir_stack) MenhirState103
                        | LP ->
                            _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState103
                        | RETURN ->
                            _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState103
                        | SKIP ->
                            _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState103
                        | WHILE ->
                            _menhir_run18 _menhir_env (Obj.magic _menhir_stack) MenhirState103
                        | _ ->
                            assert (not _menhir_env._menhir_error);
                            _menhir_env._menhir_error <- true;
                            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState103)
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let ((_menhir_stack, _menhir_s, _), _) = _menhir_stack in
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | TIMES ->
            _menhir_run37 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_list_input_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((int * Serverast.url * Serverast.ident * Serverast.value) list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState209 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | PERFORM ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | INT _v ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_stack = (_menhir_stack, _v) in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | STEPS ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | EOF ->
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let (((_menhir_stack, _menhir_s, (l : ((Semantics.Umap.key * Serverast.program *
   (Semantics.Server.Mem.key * Serverast.value) list)
  list))), _, (l2 : ((int * Serverast.url * Serverast.ident * Serverast.value) list))), (i : (
# 27 "parser.mly"
       (int)
# 3562 "parser.ml"
                        ))) = _menhir_stack in
                        let _9 = () in
                        let _8 = () in
                        let _6 = () in
                        let _4 = () in
                        let _3 = () in
                        let _2 = () in
                        let _v : (Semantics.configuration *
  (int * Serverast.url * Serverast.ident * Serverast.value) list * int) = 
# 58 "parser.mly"
    ({ ss = ss_of_list l;
       cc = Dynqueue.empty;
       s = Umap.empty;
       l = Uset.empty}, l2, i)
# 3577 "parser.ml"
                         in
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let (_1 : (Semantics.configuration *
  (int * Serverast.url * Serverast.ident * Serverast.value) list * int)) = _v in
                        Obj.magic _1
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let ((_menhir_stack, _menhir_s, _), _) = _menhir_stack in
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let ((_menhir_stack, _menhir_s, _), _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState224 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (int * Serverast.url * Serverast.ident * Serverast.value))), _, (xs : ((int * Serverast.url * Serverast.ident * Serverast.value) list))) = _menhir_stack in
        let _v : ((int * Serverast.url * Serverast.ident * Serverast.value) list) = 
# 213 "<standard.mly>"
    ( x :: xs )
# 3615 "parser.ml"
         in
        _menhir_goto_list_input_ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_goto_prog : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.program) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState184 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (p1 : (Serverast.program))), _, (p2 : (Serverast.program))) = _menhir_stack in
        let _2 = () in
        let _v : (Serverast.program) = 
# 74 "parser.mly"
                                    (Sequence (p1,p2))
# 3633 "parser.ml"
         in
        _menhir_goto_prog _menhir_env _menhir_stack _menhir_s _v
    | MenhirState179 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | ELSE ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run180 _menhir_env (Obj.magic _menhir_stack) MenhirState187 _v
            | IF ->
                _menhir_run177 _menhir_env (Obj.magic _menhir_stack) MenhirState187
            | LP ->
                _menhir_run176 _menhir_env (Obj.magic _menhir_stack) MenhirState187
            | RETURN ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState187
            | SKIP ->
                _menhir_run172 _menhir_env (Obj.magic _menhir_stack) MenhirState187
            | WHILE ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState187
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState187)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState176 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (p : (Serverast.program))) = _menhir_stack in
            let _3 = () in
            let _1 = () in
            let _v : (Serverast.program) = 
# 77 "parser.mly"
                   (p)
# 3683 "parser.ml"
             in
            _menhir_goto_unprog _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState3 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _menhir_stack = Obj.magic _menhir_stack in
            let x = () in
            let _v : (unit option) = 
# 116 "<standard.mly>"
    ( Some x )
# 3705 "parser.ml"
             in
            _menhir_goto_option_WITH_ _menhir_env _menhir_stack _v
        | MEMORY ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _v : (unit option) = 
# 114 "<standard.mly>"
    ( None )
# 3713 "parser.ml"
             in
            _menhir_goto_option_WITH_ _menhir_env _menhir_stack _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_run18 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState18 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState18 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState18 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState18
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState18

and _menhir_run83 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState83 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState83 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState83 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState83
    | COMMA | ELSE | MEMORY | RB | RP | SEMICOLON | WITH ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.t) = 
# 138 "parser.mly"
         (Tskip (Dvalue (String "")))
# 3805 "parser.ml"
         in
        _menhir_goto_unt _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState83

and _menhir_run85 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState85 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState85 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState85 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState85
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState85

and _menhir_run87 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | CALL ->
        _menhir_run94 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | IDENT _v ->
        _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState87 _v
    | IF ->
        _menhir_run88 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | LP ->
        _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | RETURN ->
        _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | SKIP ->
        _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | WHILE ->
        _menhir_run18 _menhir_env (Obj.magic _menhir_stack) MenhirState87
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState87

and _menhir_run88 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DOLLAR ->
        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | FST ->
        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | IDENT _v ->
        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState88 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState88 _v
    | LB ->
        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | LP ->
        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | MINUS ->
        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | NOT ->
        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | SND ->
        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState88 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState88
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState88

and _menhir_run91 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 26 "parser.mly"
       (string)
# 3919 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DEF ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DOLLAR ->
            _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | FALSE ->
            _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | FST ->
            _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | IDENT _v ->
            _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState92 _v
        | INT _v ->
            _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState92 _v
        | LB ->
            _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | LP ->
            _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | MINUS ->
            _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | NOT ->
            _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | SND ->
            _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | STRING _v ->
            _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState92 _v
        | TRUE ->
            _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | UNDEFINED ->
            _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | VLOGIN ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState92
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState92)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run94 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_stack = (_menhir_stack, _v) in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | DEF ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | DOLLAR ->
                        _menhir_run29 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | FALSE ->
                        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | FST ->
                        _menhir_run27 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | IDENT _v ->
                        _menhir_run25 _menhir_env (Obj.magic _menhir_stack) MenhirState98 _v
                    | INT _v ->
                        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState98 _v
                    | LB ->
                        _menhir_run23 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | LP ->
                        _menhir_run22 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | MINUS ->
                        _menhir_run21 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | NOT ->
                        _menhir_run20 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | SND ->
                        _menhir_run19 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | STRING _v ->
                        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState98 _v
                    | TRUE ->
                        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | UNDEFINED ->
                        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | VLOGIN ->
                        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState98
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState98)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_loption_separated_nonempty_list_SEMICOLON_expr__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.expression list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | RB ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (xs : (Serverast.expression list))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (Serverast.expression) = let l = 
# 232 "<standard.mly>"
    ( xs )
# 4072 "parser.ml"
         in
        
# 102 "parser.mly"
                                             (pair_of_list l)
# 4077 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_ntexpr : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.expression) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState118 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.expression) = 
# 107 "parser.mly"
                   (Op ("fst", [e]))
# 4099 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState151 | MenhirState117 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | COMMA ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | FALSE ->
                _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | FST ->
                _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | IDENT _v ->
                _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState151 _v
            | INT _v ->
                _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState151 _v
            | LB ->
                _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | LP ->
                _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | MINUS ->
                _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | NOT ->
                _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | SND ->
                _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | STRING _v ->
                _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState151 _v
            | TRUE ->
                _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | UNDEFINED ->
                _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | VLOGIN ->
                _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState151
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState151)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run149 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (x : (Serverast.expression))) = _menhir_stack in
            let _v : (Serverast.expression list) = 
# 241 "<standard.mly>"
    ( [ x ] )
# 4176 "parser.ml"
             in
            _menhir_goto_separated_nonempty_list_COMMA_ntexpr_ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState123 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.expression) = let o = 
# 115 "parser.mly"
          ("times")
# 4193 "parser.ml"
         in
        
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4198 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState125 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | NEQ | OR | PERFORM | PLUS | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 114 "parser.mly"
         ("plus")
# 4219 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4224 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState127 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.expression) = let o = 
# 120 "parser.mly"
        ("mod")
# 4241 "parser.ml"
         in
        
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4246 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState129 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.expression) = let o = 
# 119 "parser.mly"
        ("div")
# 4257 "parser.ml"
         in
        
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4262 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState131 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AT | COMMA | DO | ELSE | HAT | MEMORY | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 117 "parser.mly"
       ("or")
# 4301 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4306 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState133 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | HAT | MEMORY | NEQ | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 121 "parser.mly"
        ("neq")
# 4345 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4350 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState135 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | NEQ | OR | PERFORM | PLUS | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 113 "parser.mly"
          ("minus")
# 4377 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4382 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState137 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 122 "parser.mly"
        ("leq")
# 4413 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4418 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState139 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 123 "parser.mly"
      ("l")
# 4449 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4454 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState141 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 124 "parser.mly"
        ("geq")
# 4485 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4490 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState143 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | NEQ | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 125 "parser.mly"
      ("g")
# 4521 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4526 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState145 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | HAT | MEMORY | NEQ | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 118 "parser.mly"
       ("eq")
# 4565 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4570 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState147 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | HAT | MEMORY | OR | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 116 "parser.mly"
        ("and")
# 4613 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4618 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState149 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AT | COMMA | DO | ELSE | HAT | MEMORY | PERFORM | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s, (e1 : (Serverast.expression))), _, (e2 : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = let o = 
# 112 "parser.mly"
        ("concat")
# 4665 "parser.ml"
             in
            
# 106 "parser.mly"
                                   (Op (o, [e1;e2]))
# 4670 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState181 | MenhirState174 | MenhirState164 | MenhirState16 | MenhirState160 | MenhirState115 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run149 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | COMMA | ELSE | MEMORY | RB | RP | SEMICOLON | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (e : (Serverast.expression))) = _menhir_stack in
            let _v : (Serverast.expression) = 
# 86 "parser.mly"
               (e)
# 4718 "parser.ml"
             in
            _menhir_goto_expr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState15 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AND | AT | COMMA | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | NEQ | OR | PERFORM | PLUS | RB | RP | SEMICOLON | THEN | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.expression) = 
# 104 "parser.mly"
                     (Op ("minus", [e]))
# 4745 "parser.ml"
             in
            _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState14 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.expression) = 
# 105 "parser.mly"
                   (Op ("not", [e]))
# 4762 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState13 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.expression) = 
# 108 "parser.mly"
                   (Op ("snd", [e]))
# 4773 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState4 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | DO ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run180 _menhir_env (Obj.magic _menhir_stack) MenhirState171 _v
            | IF ->
                _menhir_run177 _menhir_env (Obj.magic _menhir_stack) MenhirState171
            | LP ->
                _menhir_run176 _menhir_env (Obj.magic _menhir_stack) MenhirState171
            | RETURN ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState171
            | SKIP ->
                _menhir_run172 _menhir_env (Obj.magic _menhir_stack) MenhirState171
            | WHILE ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState171
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState171)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run149 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState172 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run149 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | ELSE | MEMORY | RP | SEMICOLON | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))) = _menhir_stack in
            let _1 = () in
            let _v : (Serverast.program) = 
# 81 "parser.mly"
                    (Skip e)
# 4876 "parser.ml"
             in
            _menhir_goto_unprog _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState177 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run149 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | THEN ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run180 _menhir_env (Obj.magic _menhir_stack) MenhirState179 _v
            | IF ->
                _menhir_run177 _menhir_env (Obj.magic _menhir_stack) MenhirState179
            | LP ->
                _menhir_run176 _menhir_env (Obj.magic _menhir_stack) MenhirState179
            | RETURN ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState179
            | SKIP ->
                _menhir_run172 _menhir_env (Obj.magic _menhir_stack) MenhirState179
            | WHILE ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState179
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState179)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState217 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | AND ->
            _menhir_run147 _menhir_env (Obj.magic _menhir_stack)
        | DIV ->
            _menhir_run129 _menhir_env (Obj.magic _menhir_stack)
        | EQ ->
            _menhir_run145 _menhir_env (Obj.magic _menhir_stack)
        | G ->
            _menhir_run143 _menhir_env (Obj.magic _menhir_stack)
        | GEQ ->
            _menhir_run141 _menhir_env (Obj.magic _menhir_stack)
        | HAT ->
            _menhir_run149 _menhir_env (Obj.magic _menhir_stack)
        | L ->
            _menhir_run139 _menhir_env (Obj.magic _menhir_stack)
        | LEQ ->
            _menhir_run137 _menhir_env (Obj.magic _menhir_stack)
        | MINUS ->
            _menhir_run135 _menhir_env (Obj.magic _menhir_stack)
        | MOD ->
            _menhir_run127 _menhir_env (Obj.magic _menhir_stack)
        | NEQ ->
            _menhir_run133 _menhir_env (Obj.magic _menhir_stack)
        | OR ->
            _menhir_run131 _menhir_env (Obj.magic _menhir_stack)
        | PLUS ->
            _menhir_run125 _menhir_env (Obj.magic _menhir_stack)
        | TIMES ->
            _menhir_run123 _menhir_env (Obj.magic _menhir_stack)
        | AT | PERFORM ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((((_menhir_stack, _menhir_s), (i : (
# 27 "parser.mly"
       (int)
# 4983 "parser.ml"
            ))), (u : (
# 26 "parser.mly"
       (string)
# 4987 "parser.ml"
            ))), (x : (
# 26 "parser.mly"
       (string)
# 4991 "parser.ml"
            ))), _, (v : (Serverast.expression))) = _menhir_stack in
            let _8 = () in
            let _6 = () in
            let _4 = () in
            let _2 = () in
            let _1 = () in
            let _v : (int * Serverast.url * Serverast.ident * Serverast.value) = 
# 64 "parser.mly"
    ((i,u,x,Server.eval Server.Mem.empty v))
# 5001 "parser.ml"
             in
            let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
            let _menhir_stack = Obj.magic _menhir_stack in
            assert (not _menhir_env._menhir_error);
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | AT ->
                _menhir_run210 _menhir_env (Obj.magic _menhir_stack) MenhirState224
            | PERFORM ->
                _menhir_reduce33 _menhir_env (Obj.magic _menhir_stack) MenhirState224
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState224)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_goto_loption_separated_nonempty_list_COMMA_ntexpr__ : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.expression list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | RP ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (f : (
# 26 "parser.mly"
       (string)
# 5039 "parser.ml"
        ))), _, (xs : (Serverast.expression list))) = _menhir_stack in
        let _4 = () in
        let _2 = () in
        let _v : (Serverast.expression) = let l = 
# 232 "<standard.mly>"
    ( xs )
# 5046 "parser.ml"
         in
        
# 109 "parser.mly"
                                                     (Op (f,l))
# 5051 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_value : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.value) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState98 | MenhirState92 | MenhirState88 | MenhirState85 | MenhirState83 | MenhirState18 | MenhirState19 | MenhirState20 | MenhirState21 | MenhirState75 | MenhirState22 | MenhirState71 | MenhirState23 | MenhirState65 | MenhirState63 | MenhirState61 | MenhirState59 | MenhirState57 | MenhirState55 | MenhirState53 | MenhirState51 | MenhirState49 | MenhirState47 | MenhirState45 | MenhirState43 | MenhirState41 | MenhirState39 | MenhirState37 | MenhirState26 | MenhirState27 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (v : (Serverast.value)) = _v in
        let _v : (Serverast.doll) = 
# 145 "parser.mly"
              (Dvalue v)
# 5071 "parser.ml"
         in
        _menhir_goto_doll _menhir_env _menhir_stack _menhir_s _v
    | MenhirState217 | MenhirState181 | MenhirState177 | MenhirState174 | MenhirState172 | MenhirState4 | MenhirState13 | MenhirState14 | MenhirState15 | MenhirState164 | MenhirState16 | MenhirState160 | MenhirState115 | MenhirState151 | MenhirState149 | MenhirState147 | MenhirState145 | MenhirState143 | MenhirState141 | MenhirState139 | MenhirState137 | MenhirState135 | MenhirState133 | MenhirState131 | MenhirState129 | MenhirState127 | MenhirState125 | MenhirState123 | MenhirState117 | MenhirState118 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (v : (Serverast.value)) = _v in
        let _v : (Serverast.expression) = 
# 99 "parser.mly"
              (Value v)
# 5081 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | MenhirState199 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (v : (Serverast.value)) = _v in
        let (_menhir_stack, _menhir_s, (x : (
# 26 "parser.mly"
       (string)
# 5091 "parser.ml"
        ))) = _menhir_stack in
        let _2 = () in
        let _v : (Semantics.Server.Mem.key * Serverast.value) = 
# 70 "parser.mly"
                              ((x,v))
# 5097 "parser.ml"
         in
        let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IDENT _v ->
            _menhir_run198 _menhir_env (Obj.magic _menhir_stack) MenhirState202 _v
        | SERVER | WITH ->
            _menhir_reduce31 _menhir_env (Obj.magic _menhir_stack) MenhirState202
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState202)
    | _ ->
        _menhir_fail ()

and _menhir_fail : unit -> 'a =
  fun () ->
    Printf.fprintf stderr "Internal failure -- please contact the parser generator's developers.\n%!";
    assert false

and _menhir_reduce33 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : ((int * Serverast.url * Serverast.ident * Serverast.value) list) = 
# 211 "<standard.mly>"
    ( [] )
# 5125 "parser.ml"
     in
    _menhir_goto_list_input_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run210 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | TIME ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | INT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | CALL ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | IDENT _v ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_stack = (_menhir_stack, _v) in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | WITH ->
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let _menhir_env = _menhir_discard _menhir_env in
                        let _tok = _menhir_env._menhir_token in
                        (match _tok with
                        | IDENT _v ->
                            let _menhir_stack = Obj.magic _menhir_stack in
                            let _menhir_stack = (_menhir_stack, _v) in
                            let _menhir_env = _menhir_discard _menhir_env in
                            let _tok = _menhir_env._menhir_token in
                            (match _tok with
                            | DEF ->
                                let _menhir_stack = Obj.magic _menhir_stack in
                                let _menhir_env = _menhir_discard _menhir_env in
                                let _tok = _menhir_env._menhir_token in
                                (match _tok with
                                | FALSE ->
                                    _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | FST ->
                                    _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | IDENT _v ->
                                    _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState217 _v
                                | INT _v ->
                                    _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState217 _v
                                | LB ->
                                    _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | LP ->
                                    _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | MINUS ->
                                    _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | NOT ->
                                    _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | SND ->
                                    _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | STRING _v ->
                                    _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState217 _v
                                | TRUE ->
                                    _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | UNDEFINED ->
                                    _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | VLOGIN ->
                                    _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState217
                                | _ ->
                                    assert (not _menhir_env._menhir_error);
                                    _menhir_env._menhir_error <- true;
                                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState217)
                            | _ ->
                                assert (not _menhir_env._menhir_error);
                                _menhir_env._menhir_error <- true;
                                let _menhir_stack = Obj.magic _menhir_stack in
                                let ((((_menhir_stack, _menhir_s), _), _), _) = _menhir_stack in
                                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                        | _ ->
                            assert (not _menhir_env._menhir_error);
                            _menhir_env._menhir_error <- true;
                            let _menhir_stack = Obj.magic _menhir_stack in
                            let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
                            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        let _menhir_stack = Obj.magic _menhir_stack in
                        let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_goto_unprog : _menhir_env -> 'ttv_tail -> _menhir_state -> (Serverast.program) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState3 | MenhirState176 | MenhirState184 | MenhirState179 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | SEMICOLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run180 _menhir_env (Obj.magic _menhir_stack) MenhirState184 _v
            | IF ->
                _menhir_run177 _menhir_env (Obj.magic _menhir_stack) MenhirState184
            | LP ->
                _menhir_run176 _menhir_env (Obj.magic _menhir_stack) MenhirState184
            | RETURN ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState184
            | SKIP ->
                _menhir_run172 _menhir_env (Obj.magic _menhir_stack) MenhirState184
            | WHILE ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState184
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState184)
        | ELSE | MEMORY | RP | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, (p : (Serverast.program))) = _menhir_stack in
            let _v : (Serverast.program) = 
# 73 "parser.mly"
               (p)
# 5282 "parser.ml"
             in
            _menhir_goto_prog _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | MenhirState187 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))), _, (p1 : (Serverast.program))), _, (p2 : (Serverast.program))) = _menhir_stack in
        let _5 = () in
        let _3 = () in
        let _1 = () in
        let _v : (Serverast.program) = 
# 79 "parser.mly"
                                                  (If (e,p1,p2))
# 5301 "parser.ml"
         in
        _menhir_goto_unprog _menhir_env _menhir_stack _menhir_s _v
    | MenhirState171 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), _, (e : (Serverast.expression))), _, (p : (Serverast.program))) = _menhir_stack in
        let _3 = () in
        let _1 = () in
        let _v : (Serverast.program) = 
# 80 "parser.mly"
                                   (While (e,p))
# 5313 "parser.ml"
         in
        _menhir_goto_unprog _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        _menhir_fail ()

and _menhir_run5 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (u : (
# 26 "parser.mly"
       (string)
# 5332 "parser.ml"
        )) = _v in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.value) = 
# 95 "parser.mly"
                     (Login u)
# 5339 "parser.ml"
         in
        _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
    | LP ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | IDENT _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | RP ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), (u : (
# 26 "parser.mly"
       (string)
# 5360 "parser.ml"
                ))) = _menhir_stack in
                let _4 = () in
                let _2 = () in
                let _1 = () in
                let _v : (Serverast.value) = 
# 94 "parser.mly"
                           (Login u)
# 5368 "parser.ml"
                 in
                _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run10 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (Serverast.value) = 
# 96 "parser.mly"
              (Undefined)
# 5398 "parser.ml"
     in
    _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v

and _menhir_run11 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (Serverast.value) = 
# 91 "parser.mly"
         (Bool true)
# 5410 "parser.ml"
     in
    _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v

and _menhir_run17 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | CALL ->
        _menhir_run94 _menhir_env (Obj.magic _menhir_stack) MenhirState17
    | IDENT _v ->
        _menhir_run91 _menhir_env (Obj.magic _menhir_stack) MenhirState17 _v
    | IF ->
        _menhir_run88 _menhir_env (Obj.magic _menhir_stack) MenhirState17
    | LP ->
        _menhir_run87 _menhir_env (Obj.magic _menhir_stack) MenhirState17
    | RETURN ->
        _menhir_run85 _menhir_env (Obj.magic _menhir_stack) MenhirState17
    | SKIP ->
        _menhir_run83 _menhir_env (Obj.magic _menhir_stack) MenhirState17
    | WHILE ->
        _menhir_run18 _menhir_env (Obj.magic _menhir_stack) MenhirState17
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState17

and _menhir_run12 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 28 "parser.mly"
       (string)
# 5442 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (s : (
# 28 "parser.mly"
       (string)
# 5450 "parser.ml"
    )) = _v in
    let _v : (Serverast.value) = 
# 93 "parser.mly"
               (String s)
# 5455 "parser.ml"
     in
    _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v

and _menhir_run13 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState13 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState13 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState13 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState13
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState13

and _menhir_run14 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState14 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState14 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState14 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState14
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState14

and _menhir_run15 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState15 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState15 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState15 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState15
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState15

and _menhir_run16 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState16 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState16 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState16 _v
    | TILDE ->
        _menhir_run17 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState16
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState16

and _menhir_run115 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState115 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState115 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState115 _v
    | TILDE ->
        _menhir_run17 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState115
    | RB ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_s = MenhirState115 in
        let _v : (Serverast.expression list) = 
# 142 "<standard.mly>"
    ( [] )
# 5649 "parser.ml"
         in
        _menhir_goto_loption_separated_nonempty_list_SEMICOLON_expr__ _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState115

and _menhir_run24 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 27 "parser.mly"
       (int)
# 5660 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (i : (
# 27 "parser.mly"
       (int)
# 5668 "parser.ml"
    )) = _v in
    let _v : (Serverast.value) = 
# 90 "parser.mly"
            (Int i)
# 5673 "parser.ml"
     in
    _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v

and _menhir_run116 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 26 "parser.mly"
       (string)
# 5680 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | LP ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | FALSE ->
            _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | FST ->
            _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | IDENT _v ->
            _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState117 _v
        | INT _v ->
            _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState117 _v
        | LB ->
            _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | LP ->
            _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | MINUS ->
            _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | NOT ->
            _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | SND ->
            _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | STRING _v ->
            _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState117 _v
        | TRUE ->
            _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | UNDEFINED ->
            _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | VLOGIN ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState117
        | RP ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_s = MenhirState117 in
            let _v : (Serverast.expression list) = 
# 142 "<standard.mly>"
    ( [] )
# 5724 "parser.ml"
             in
            _menhir_goto_loption_separated_nonempty_list_COMMA_ntexpr__ _menhir_env _menhir_stack _menhir_s _v
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState117)
    | AND | AT | COMMA | DIV | DO | ELSE | EQ | G | GEQ | HAT | L | LEQ | MEMORY | MINUS | MOD | NEQ | OR | PERFORM | PLUS | RB | RP | SEMICOLON | THEN | TIMES | WITH ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, (x : (
# 26 "parser.mly"
       (string)
# 5736 "parser.ml"
        ))) = _menhir_stack in
        let _v : (Serverast.expression) = 
# 100 "parser.mly"
              (Variable x)
# 5741 "parser.ml"
         in
        _menhir_goto_ntexpr _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_run118 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState118 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState118 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState118 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState118
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState118

and _menhir_run28 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let _1 = () in
    let _v : (Serverast.value) = 
# 92 "parser.mly"
          (Bool false)
# 5796 "parser.ml"
     in
    _menhir_goto_value _menhir_env _menhir_stack _menhir_s _v

and _menhir_goto_list_server_ : _menhir_env -> 'ttv_tail -> _menhir_state -> ((Semantics.Umap.key * Serverast.program *
   (Semantics.Server.Mem.key * Serverast.value) list)
  list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    match _menhir_s with
    | MenhirState204 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, (x : (Semantics.Umap.key * Serverast.program *
  (Semantics.Server.Mem.key * Serverast.value) list))), _, (xs : ((Semantics.Umap.key * Serverast.program *
   (Semantics.Server.Mem.key * Serverast.value) list)
  list))) = _menhir_stack in
        let _v : ((Semantics.Umap.key * Serverast.program *
   (Semantics.Server.Mem.key * Serverast.value) list)
  list) = 
# 213 "<standard.mly>"
    ( x :: xs )
# 5818 "parser.ml"
         in
        _menhir_goto_list_server_ _menhir_env _menhir_stack _menhir_s _v
    | MenhirState0 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        assert (not _menhir_env._menhir_error);
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | WITH ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | USER ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | INPUTS ->
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let _menhir_env = _menhir_discard _menhir_env in
                    let _tok = _menhir_env._menhir_token in
                    (match _tok with
                    | AT ->
                        _menhir_run210 _menhir_env (Obj.magic _menhir_stack) MenhirState209
                    | PERFORM ->
                        _menhir_reduce33 _menhir_env (Obj.magic _menhir_stack) MenhirState209
                    | _ ->
                        assert (not _menhir_env._menhir_error);
                        _menhir_env._menhir_error <- true;
                        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState209)
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    let _menhir_stack = Obj.magic _menhir_stack in
                    let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_menhir_stack, _menhir_s, _) = _menhir_stack in
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s, _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        _menhir_fail ()

and _menhir_run4 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState4 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState4 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState4 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState4
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState4

and _menhir_run172 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState172 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState172 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState172 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState172
    | ELSE | MEMORY | RP | SEMICOLON | WITH ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        let _1 = () in
        let _v : (Serverast.program) = 
# 82 "parser.mly"
         (Skip (Value (String "")))
# 5946 "parser.ml"
         in
        _menhir_goto_unprog _menhir_env _menhir_stack _menhir_s _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState172

and _menhir_run174 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState174 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState174 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState174 _v
    | TILDE ->
        _menhir_run17 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState174
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState174

and _menhir_run176 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        _menhir_run180 _menhir_env (Obj.magic _menhir_stack) MenhirState176 _v
    | IF ->
        _menhir_run177 _menhir_env (Obj.magic _menhir_stack) MenhirState176
    | LP ->
        _menhir_run176 _menhir_env (Obj.magic _menhir_stack) MenhirState176
    | RETURN ->
        _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState176
    | SKIP ->
        _menhir_run172 _menhir_env (Obj.magic _menhir_stack) MenhirState176
    | WHILE ->
        _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState176
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState176

and _menhir_run177 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | FALSE ->
        _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | FST ->
        _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | IDENT _v ->
        _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState177 _v
    | INT _v ->
        _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState177 _v
    | LB ->
        _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | LP ->
        _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | MINUS ->
        _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | NOT ->
        _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | SND ->
        _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | STRING _v ->
        _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState177 _v
    | TRUE ->
        _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | UNDEFINED ->
        _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | VLOGIN ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState177
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState177

and _menhir_run180 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 26 "parser.mly"
       (string)
# 6056 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | DEF ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | FALSE ->
            _menhir_run28 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | FST ->
            _menhir_run118 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | IDENT _v ->
            _menhir_run116 _menhir_env (Obj.magic _menhir_stack) MenhirState181 _v
        | INT _v ->
            _menhir_run24 _menhir_env (Obj.magic _menhir_stack) MenhirState181 _v
        | LB ->
            _menhir_run115 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | LP ->
            _menhir_run16 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | MINUS ->
            _menhir_run15 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | NOT ->
            _menhir_run14 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | SND ->
            _menhir_run13 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | STRING _v ->
            _menhir_run12 _menhir_env (Obj.magic _menhir_stack) MenhirState181 _v
        | TILDE ->
            _menhir_run17 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | TRUE ->
            _menhir_run11 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | UNDEFINED ->
            _menhir_run10 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | VLOGIN ->
            _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState181
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState181)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_errorcase : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    match _menhir_s with
    | MenhirState224 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState217 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((((_menhir_stack, _menhir_s), _), _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState209 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState204 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState202 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState199 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState197 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s, _), _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState187 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState184 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState181 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState179 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState177 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState176 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState174 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState172 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState171 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState164 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState160 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState151 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState149 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState147 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState145 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState143 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState141 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState139 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState137 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState135 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState133 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState131 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState129 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState127 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState125 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState123 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState118 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState117 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState115 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState109 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState106 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState103 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s, _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState98 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (((_menhir_stack, _menhir_s), _), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState92 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState90 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState88 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState87 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState85 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState83 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState82 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState75 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState71 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState65 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState63 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState61 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState59 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState57 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState55 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState53 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState51 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState49 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState47 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState45 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState43 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState41 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState39 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState37 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState27 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState26 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState23 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState22 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState21 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState20 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState19 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState18 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState17 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState16 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState15 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState14 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState13 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState4 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState3 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState0 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        raise _eRR

and _menhir_reduce35 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _v : ((Semantics.Umap.key * Serverast.program *
   (Semantics.Server.Mem.key * Serverast.value) list)
  list) = 
# 211 "<standard.mly>"
    ( [] )
# 6437 "parser.ml"
     in
    _menhir_goto_list_server_ _menhir_env _menhir_stack _menhir_s _v

and _menhir_run1 : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    let _menhir_stack = (_menhir_stack, _menhir_s) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | IDENT _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | COLON ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | IDENT _v ->
                _menhir_run180 _menhir_env (Obj.magic _menhir_stack) MenhirState3 _v
            | IF ->
                _menhir_run177 _menhir_env (Obj.magic _menhir_stack) MenhirState3
            | LP ->
                _menhir_run176 _menhir_env (Obj.magic _menhir_stack) MenhirState3
            | RETURN ->
                _menhir_run174 _menhir_env (Obj.magic _menhir_stack) MenhirState3
            | SKIP ->
                _menhir_run172 _menhir_env (Obj.magic _menhir_stack) MenhirState3
            | WHILE ->
                _menhir_run4 _menhir_env (Obj.magic _menhir_stack) MenhirState3
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState3)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let ((_menhir_stack, _menhir_s), _) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s

and _menhir_discard : _menhir_env -> _menhir_env =
  fun _menhir_env ->
    let lexer = _menhir_env._menhir_lexer in
    let lexbuf = _menhir_env._menhir_lexbuf in
    let _tok = lexer lexbuf in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    }

and debut : (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Semantics.configuration *
  (int * Serverast.url * Serverast.ident * Serverast.value) list * int) =
  fun lexer lexbuf ->
    let _menhir_env = let _tok = Obj.magic () in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    } in
    Obj.magic (let _menhir_stack = ((), _menhir_env._menhir_lexbuf.Lexing.lex_curr_p) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | SERVER ->
        _menhir_run1 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | WITH ->
        _menhir_reduce35 _menhir_env (Obj.magic _menhir_stack) MenhirState0
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState0)

# 269 "<standard.mly>"
  

# 6525 "parser.ml"
