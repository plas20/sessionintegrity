


module Client = struct

  open Clientast

  module Mem = Map.Make(String)
  type memory = value Mem.t

  let rec eval mu = function
      Value v -> v
    | Variable x -> (try Mem.find x mu with Not_found -> Undefined)
    | Op (o,l) -> match o, List.map (eval mu) l with
                    "plus",[Int n1;Int n2] -> Int (n1+n2)
                  | "plus", [Dummy ; Int n2] -> Int (n2+1)
                  | "plus", [Int n1 ; Dummy] -> Int (n1+1)
                  | "plus", [Dummy ; Dummy] -> Int 2
                  | "minus", [Int n] -> Int (-n)
                  | "minus", [Dummy] -> Int (-1)
                  | "minus", [Int n1; Int n2] -> Int (n1-n2)
                  | "minus", [Int n1; Dummy] -> Int (n1-1)
                  | "minus", [Dummy; Int n2] -> Int (1-n2)
                  | "minus", [Dummy;Dummy] -> Int 0
                  | "times", [Int n1; Int n2] -> Int (n1*n2)
                  | "times", [Int n;Dummy]
                    | "times", [Dummy; Int n] -> Int n
                  | "times", [Dummy;Dummy] -> Int 1
                  | "div", [Int n1;Int n2] when n2 <> 0 -> Int (n1/n2)
                  | "div", [Int n;Dummy] -> Int n
                  | "div", [Dummy;Int n] -> Int (1/n)
                  | "div", [Dummy;Dummy] -> Int 1
                  | "mod", [Int n1;Int n2] when n2 <> 0-> Int (n1 mod n2)
                  | "mod", [Int n; Dummy] -> Int (n mod 1)
                  | "mod" , [Dummy;Int n] -> Int (1 mod n)
                  | "mod", [Dummy;Dummy] -> Int (1 mod 1)
                  | "and", [Bool b1;Bool b2] -> Bool (b1 && b2)
                  | "and", [Dummy;Bool b]
                    | "and", [Bool b;Dummy] -> Bool b
                  | "and", [Dummy;Dummy] -> Bool true
                  | "or", [Bool b1;Bool b2] -> Bool (b1 || b2)
                  | "or", [Dummy;Bool _]
                    | "or", [Bool _;Dummy]
                    | "or", [Dummy;Dummy] -> Bool true
                  | "not", [Bool b] -> Bool (not b)
                  | "not", [Dummy] -> Bool false
                  | "pair", [v1;v2] -> Pair (v1,v2)
                  | "fst", [Pair(v1,_)] -> v1
                  | "fst", [Dummy] -> Dummy
                  | "snd", [Pair(_,v2)] -> v2
                  | "snd", [Dummy] -> Dummy
                  | "eq", [Dummy;Int n]
                    | "eq", [Int n;Dummy] -> Bool (n=1)
                  | "eq", [Dummy; String s]
                    | "eq", [String s;Dummy] -> Bool (s="")
                  | "eq", [Dummy; Bool b]
                    | "eq", [Bool b;Dummy] -> Bool b
                  | "eq", [v1;v2] -> Bool (v1=v2)
                  | "neq", [v1;v2] -> eval mu (Op ("not", [Op ("eq",[Value v1;Value v2])]))
                  | "l", [Int n1;Int n2] -> Bool (n1<n2)
                  | "l", [Dummy; Int n] -> Bool (1<n)
                  | "l", [Int n; Dummy] -> Bool (n<1)
                  | "l", [Dummy;Dummy] -> Bool false
                  | "leq", [v1;v2] -> eval mu (Op ("or",[Op("l",[Value v1; Value v2]);Op("eq",[Value v1;Value v2])]))
                  | "g", [v1;v2] -> eval mu (Op ("l",[Value v2; Value v1]))
                  | "geq", [v1;v2] -> eval mu (Op ("leq",[Value v2; Value v1]))
                  | "concat", [Dummy ; String s]
                    | "concat", [String s; Dummy] -> String s
                  | "concat", [Dummy;Dummy] -> String ""
                  | "concat", [String s1;String s2] -> String (s1 ^ s2)
                  | "concat", [String s1;Int n2] ->
                     String (s1 ^ string_of_int n2)
                  | "concat", [Int n1 ; String s2] ->
                     String (string_of_int n1 ^ s2)
                  | "concat", [Int n1 ; Int n2] ->
                     String (string_of_int n1 ^ string_of_int n2)
                  | "random", [] -> Int (Random.int 10)
                  | "hd", [ Pair (v1, _) ] -> v1
                  | "hd", [Dummy] -> Dummy
                  | "tl", [ Pair (_, v2) ] -> v2
                  | "tl", [Dummy] -> Dummy
                  | "add", [v ;  Pair (add_here, take_from_here) ] -> Pair(Pair(v,add_here) , take_from_here)
                  | "add", [ v ; Undefined ] -> Pair(Pair(v,Undefined),Undefined)
                  | "take", [Pair (add_here,take_from_here)] ->
                     if take_from_here = Undefined
                     then let take_from_here = 
                            eval mu (Op ("rev", [Value add_here]))
                          in let t,q = eval mu (Op ("fst", [Value take_from_here])),
                                       eval mu (Op ("snd", [Value take_from_here])) in
                             Pair (t, Pair (Undefined,q))
                     else let t,q = eval mu (Op ("fst", [Value take_from_here])),
                                    eval mu (Op ("snd", [Value take_from_here])) in
                          Pair (t, Pair(add_here, q))
                  | "rev", [ l ] -> eval mu (Op ("auxrev", [ Value Undefined ; Value l ]))
                  | "auxrev", [acc ; Undefined] -> acc
                  | "auxrev", [acc ; Pair (t,q)] -> eval mu (Op ("auxrev", [Value (Pair(t,acc)) ;
                                                                            Value q]))
                  | _ -> Undefined

  type configuration =
    |NT of program * memory
    |T of value * memory 
    |U of memory

  type flag =
    Fnone
  | Fyield of program
  | Fcall of url * ident * value * ident * program
  | Fsilent of url * ident * value * ident * program
  | Fcb of ident * program * value
  | Flogin of url * program * program
    
  let rec step mu = function
      Return e -> T (eval mu e,mu),Fnone
    | Assign (x,e) -> U (Mem.add x (eval mu e) mu),Fnone
    | Yield -> U mu, Fyield (Skip (Value (String "")))
    | Sequence (p1,p2) ->
       begin match step mu p1 with
       | U mu, Flogin(u,pt,pf) -> U mu, Flogin(u, Sequence(pt,p2), Sequence(pf,p2))
       | U mu, Fyield p -> U mu, Fyield (Sequence (p,p2))
       | NT (p,mu),f -> NT (Sequence (p,p2),mu),f
       | T (_,mu),f
         | U mu,f -> NT (p2,mu),f end
    | If (e,p1,p2) ->
       begin match eval mu e with
       | Bool true -> NT (p1,mu),Fnone
       | Bool false -> NT (p2,mu),Fnone
       | _ -> failwith "condition is not boolean" end
    | Iflogin (u,p1,p2) ->
       U mu, Flogin (u,p1,p2)
    | Skip s ->
       begin match eval mu s with
         String s -> print_string s
        |_ -> failwith "skip has to be a string" end ; U mu,Fnone
    | While (e,p') as p->
       begin match eval mu e with
       | Bool true -> NT (Sequence (p',p),mu), Fnone
       | Bool false -> U mu,Fnone
       | _ -> failwith "condition is not boolean" end
    | Call(u,y,e,x,p) -> U mu, Fcall (u,y,eval mu e,x,p)
    | CallSilently(u,y,e,x,p) -> U mu, Fsilent (u,y,eval mu e,x,p)
    | AddCallback(x,p,e) -> U mu, Fcb (x,p,eval mu e)
      
end

module Server = struct

  module C = Clientast
  open Serverast
     
  module Mem = Map.Make(String)
  type memory = value Mem.t

  let rec cval_of_sval = function
      Int n -> C.Int n
    | String s -> C.String s
    | Bool b -> C.Bool b
    | Pair (v1,v2) -> C.Pair (cval_of_sval v1,cval_of_sval v2)
    | Login u -> C.Login u
    | Undefined -> C.Undefined
    | Dummy -> C.Dummy
    | Vtilde _ -> failwith "tilde value given to client"

  let rec sval_of_cval = function
      C.Int n -> Int n
    | C.String s -> String s
    | C.Bool b -> Bool b
    | C.Pair (v1,v2) -> Pair (sval_of_cval v1,sval_of_cval v2)
    | C.Login u -> Login u
    | C.Undefined -> Undefined
    | C.Dummy -> Dummy
              
  let rec xi mu = function
      Dvalue v -> C.Value (cval_of_sval v)
    | Dvariable x -> C.Variable x
    | Dreference x -> (try C.Value (cval_of_sval (Mem.find x mu))
                       with Not_found -> C.Value C.Undefined)
    | Dop (o,l) -> C.Op (o,List.map (xi mu) l)

  let rec phi mu = function
    | Tassign (x,e) -> C.Assign (x,xi mu e)
    | Tsequence (t1,t2) -> C.Sequence (phi mu t1, phi mu t2)
    | Tif (e,t1,t2) -> C.If (xi mu e, phi mu t1, phi mu t2)
    | Tiflogin (u,t1,t2) -> C.Iflogin (u,phi mu t1, phi mu t2)
    | Twhile (e,t) -> C.While (xi mu e,phi mu t)
    | Tskip s -> C.Skip (xi mu s)
    | Treturn e -> C.Return (xi mu e)
    | Tcall (u,y,e,x,t) -> C.Call (u,y,xi mu e,x,phi mu t)

              
  let rec eval mu = function
      Value v -> v
    | Variable x -> (try Mem.find x mu with Not_found -> Undefined)
    | Tilde t -> Vtilde (phi mu t)
    | Op (o,l) -> match o, List.map (eval mu) l with
                    "plus",[Int n1;Int n2] -> Int (n1+n2)
                  | "plus", [Dummy ; Int n2] -> Int (n2+1)
                  | "plus", [Int n1 ; Dummy] -> Int (n1+1)
                  | "plus", [Dummy ; Dummy] -> Int 2
                  | "minus", [Int n] -> Int (-n)
                  | "minus", [Dummy] -> Int (-1)
                  | "minus", [Int n1; Int n2] -> Int (n1-n2)
                  | "minus", [Int n1; Dummy] -> Int (n1-1)
                  | "minus", [Dummy; Int n2] -> Int (1-n2)
                  | "minus", [Dummy;Dummy] -> Int 0
                  | "times", [Int n1; Int n2] -> Int (n1*n2)
                  | "times", [Int n;Dummy]
                    | "times", [Dummy; Int n] -> Int n
                  | "times", [Dummy;Dummy] -> Int 1
                  | "div", [Int n1;Int n2] when n2 <> 0 -> Int (n1/n2)
                  | "div", [Int n;Dummy] -> Int n
                  | "div", [Dummy;Int n] -> Int (1/n)
                  | "div", [Dummy;Dummy] -> Int 1
                  | "mod", [Int n1;Int n2] when n2 <> 0-> Int (n1 mod n2)
                  | "mod", [Int n; Dummy] -> Int (n mod 1)
                  | "mod" , [Dummy;Int n] -> Int (1 mod n)
                  | "mod", [Dummy;Dummy] -> Int (1 mod 1)
                  | "and", [Bool b1;Bool b2] -> Bool (b1 && b2)
                  | "and", [Dummy;Bool b]
                    | "and", [Bool b;Dummy] -> Bool b
                  | "and", [Dummy;Dummy] -> Bool true
                  | "or", [Bool b1;Bool b2] -> Bool (b1 || b2)
                  | "or", [Dummy;Bool _]
                    | "or", [Bool _;Dummy]
                    | "or", [Dummy;Dummy] -> Bool true
                  | "not", [Bool b] -> Bool (not b)
                  | "not", [Dummy] -> Bool false
                  | "pair", [v1;v2] -> Pair (v1,v2)
                  | "fst", [Pair(v1,_)] -> v1
                  | "fst", [Dummy] -> Dummy
                  | "snd", [Pair(_,v2)] -> v2
                  | "snd", [Dummy] -> Dummy
                  | "eq", [Dummy;Int n]
                    | "eq", [Int n;Dummy] -> Bool (n=1)
                  | "eq", [Dummy; String s]
                    | "eq", [String s;Dummy] -> Bool (s="")
                  | "eq", [Dummy; Bool b]
                    | "eq", [Bool b;Dummy] -> Bool b
                  | "eq", [v1;v2] -> Bool (v1=v2)
                  | "neq", [v1;v2] -> eval mu (Op ("not", [Op ("eq",[Value v1;Value v2])]))
                  | "l", [Int n1;Int n2] -> Bool (n1<n2)
                  | "l", [Dummy; Int n] -> Bool (1<n)
                  | "l", [Int n; Dummy] -> Bool (n<1)
                  | "l", [Dummy;Dummy] -> Bool false
                  | "leq", [v1;v2] -> eval mu (Op ("or",[Op("l",[Value v1; Value v2]);Op("eq",[Value v1;Value v2])]))
                  | "g", [v1;v2] -> eval mu (Op ("l",[Value v2; Value v1]))
                  | "geq", [v1;v2] -> eval mu (Op ("leq",[Value v2; Value v1]))
                  | "concat", [Dummy ; String s]
                    | "concat", [String s; Dummy] -> String s
                  | "concat", [Dummy;Dummy] -> String ""
                  | "concat", [String s1;String s2] -> String (s1 ^ s2)
                  | "concat", [String s1;Int n2] ->
                     String (s1 ^ string_of_int n2)
                  | "concat", [Int n1 ; String s2] ->
                     String (string_of_int n1 ^ s2)
                  | "concat", [Int n1 ; Int n2] ->
                     String (string_of_int n1 ^ string_of_int n2)
                  | "random", [] -> Int (Random.int 10)
                  | "hd", [ Pair (v1, _) ] -> v1
                  | "hd", [Dummy] -> Dummy
                  | "tl", [ Pair (_, v2) ] -> v2
                  | "tl", [Dummy] -> Dummy
                  | "add", [v ;  Pair (add_here, take_from_here) ] -> Pair(Pair(v,add_here) , take_from_here)
                  | "add", [ v ; Undefined ] -> Pair(Pair(v,Undefined),Undefined)
                  | "take", [Pair (add_here,take_from_here)] ->
                     if take_from_here = Undefined
                     then let take_from_here = 
                            eval mu (Op ("rev", [Value add_here]))
                          in let t,q = eval mu (Op ("fst", [Value take_from_here])),
                                       eval mu (Op ("snd", [Value take_from_here])) in
                             Pair (t, Pair (Undefined,q))
                     else let t,q = eval mu (Op ("fst", [Value take_from_here])),
                                    eval mu (Op ("snd", [Value take_from_here])) in
                          Pair (t, Pair(add_here, q))
                  | "rev", [ l ] -> eval mu (Op ("auxrev", [ Value Undefined ; Value l ]))
                  | "auxrev", [acc ; Undefined] -> acc
                  | "auxrev", [acc ; Pair (t,q)] -> eval mu (Op ("auxrev", [Value (Pair(t,acc)) ;
                                                                            Value q]))
                  | _ -> Undefined                  

  type configuration =
    |NT of program * memory
    |T of value * memory
    |U of memory

  let mem_of_conf = function | NT (_,mu) | T(_,mu) | U mu -> mu
  let sety y v = function
    | NT (p,mu) -> NT (p, Mem.add y v mu)
    | T (v,mu) -> T (v,Mem.add y v mu)
    | U mu -> U (Mem.add y v mu)
        
  let resety y muf conf =
    let y' mui = try Mem.find y mui with Not_found -> Undefined in
    match conf with
    | NT (p,mui) -> NT (p, Mem.add y (y' mui) muf)
    | T (v,mui) -> T (v, Mem.add y (y' mui) muf)
    | U mui -> U (Mem.add y (y' mui)  muf)

  let setcookie b = function
    | NT (p,mu) -> NT (p, Mem.add "logged-in" (Bool b) mu)
    | T (v,mu) -> T (v, Mem.add "logged-in" (Bool b) mu)
    | U mu -> U (Mem.add "logged-in" (Bool b) mu)
        
  let rec step mu p = match p with
      Return e -> T (eval mu e,mu)
    | Assign (x,e) -> U (Mem.add x (eval mu e) mu)
    | Sequence (p1,p2) ->
       begin match step mu p1 with
       | NT (p,mu) -> NT (Sequence (p,p2),mu)
       | T (_,mu) -> failwith "Return not at end of sequence"
       | U mu -> NT (p2,mu) end
    | If (e,p1,p2) ->
       begin match eval mu e with
       | Bool true -> NT (p1,mu)
       | Bool false -> NT (p2,mu)
       | _ -> failwith "condition is not boolean" end
    | Skip s -> begin match eval mu s with
                |String s -> print_string s
                |_ -> failwith "skip has to be a string" end ; U mu
    | While (e,p') ->
       begin match eval mu e with
       | Bool true -> NT (Sequence (p',p),mu)
       | Bool false -> U mu
       | _ -> failwith "condition is not boolean" end

  let rec step_star = function
    | NT (p,mu) -> step_star (step mu p)
    | T (v,mu) -> v,mu
    | U _ -> failwith "No return at end of sequence"
      
end      

module G = struct
  let gencc u = function
    | Serverast.Vtilde p -> Client.NT (p,Client.Mem.empty)
    | v -> Client.T (Server.cval_of_sval v,Client.Mem.empty)
end




open Serverast
let genv = function
  | Vtilde _ -> failwith "tilde value given to client"
  | v -> Server.cval_of_sval v
       
module Umap = Map.Make(String)
module Uset = Set.Make(String)
            
type event =
  | UserInit of url * ident * value
  | Output of url * ident * value
  | Input of value
  | StarOutput of url * ident * value
  | StarInput of value



               
type configuration =
  { ss : Server.configuration Umap.t ;
    cc : (Client.configuration *
            (ident * Clientast.program * Clientast.value)
              Dynqueue.t * url)
           Dynqueue.t;
    s : (event list) Umap.t;
    l : Uset.t }
  



         
module type Gencc = sig val gencc: Serverast.url ->
                                 Serverast.value ->
                                 Client.configuration end
         
