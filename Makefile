CMO = dynqueue.cmo clientast.cmo serverast.cmo semantics.cmo pprint.cmo compiler.cmo parser.cmo lexer.cmo run.cmo main.cmo
GENERATED = lexer.ml parser.ml parser.mli
BIN=webc
FLAGS=

all:	$(BIN)

$(BIN):	$(CMO)
	ocamlc $(FLAGS) -o $(BIN) $(CMO)


.SUFFIXES: .mli .ml .cmi .cmo .mll .mly

.mli.cmi:
	ocamlc $(FLAGS) -c $<

.ml.cmo:
	ocamlc $(FLAGS) -c $<

.mll.ml:
	ocamllex $<

.mly.ml:
	menhir --infer -v $<

clean:
	rm -f *.cm[iox] *.o *~ $(GENERATED) 
	rm -f parser.automaton parser.conflicts log webc

parser.ml: serverast.cmi semantics.cmo
serverast.mli: clientast.cmi

.depend depend: $(GENERATED)
	rm -f .depend
	ocamldep *.ml *.mli > .depend

include .depend
