
type url = string
type ident = string

type value =
  Int of int
| String of string
| Bool of bool
| Pair of value * value
| Login of url
| Undefined
| Dummy
type expression =
  Value of value
| Variable of ident
| Op of string * expression list
type program =
  Assign of ident * expression
| Sequence of program * program
| If of expression * program * program
| Iflogin of url * program * program
| While of expression * program
| Skip of expression
| Return of expression
| Yield
| Call of url * ident * expression * ident * program
| CallSilently of url * ident * expression * ident * program
| AddCallback of ident * program * expression

     
  
