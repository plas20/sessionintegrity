




let naive = ref true
let compiled = ref true
                
let both () = Compiler.show_compiled := true; Run.show_configurations := true

let usage = "usage: ./webc [options] file.servers"
let spec =
  [
    "--naive-only", Arg.Clear compiled, "only launch the uncompiled version";
    "--compiled-only", Arg.Clear naive,
    "only launch the secure compiled version";
    "--show-compiled", Arg.Set Compiler.show_compiled,
    " show the compilations every time code is compiled";
    "--show-configurations", Arg.Set Run.show_configurations,
    " show the successive web configurations";
    "--show", Arg.Unit both,
    "conjuction of --show-compiled and --show-configurations";
    "--debug", Arg.Set Run.debug, " show all client configurations as they progress"
  ]

let ifile = ref ""
let set_file f s = f := s

module R = Run.Web(Semantics.G)
module RC = Run.Web(Compiler)

open Lexing

let report (b,e) =
  let l = b.pos_lnum in
  let fc = b.pos_cnum - b.pos_bol + 1 in
  let lc = e.pos_cnum - b.pos_bol + 1 in
  Format.eprintf "File \"%s\", line %d, characters %d-%d:\n" !ifile l fc lc
          
let () =
  Arg.parse spec (set_file ifile) usage ;
  if !ifile = "" then begin Format.eprintf "No file to compile\n@?";
                            exit 1 end;
  if not (Filename.check_suffix !ifile ".servers") then begin
      Format.eprintf "The entry file must have extension .servers\n@?";
      Arg.usage spec usage;
      exit 1
    end;
  let c = open_in !ifile in
  let lb = Lexing.from_channel c in
  try
      let conf,l,n = Parser.debut Lexer.token lb in
      close_in c;
      if !naive then (Format.printf "@.Without compilation :@.";
                      R.run conf l n 0) ;
      if !compiled then (Format.printf "@.With compilation :@.";
                         RC.run conf l n 0)
  with
  | Lexer.Lexing_error s ->
     report (lexeme_start_p lb, lexeme_end_p lb) ;
     Format.eprintf "lexical error: %s@." s;
     exit 1
  | Parser.Error ->
     report (lexeme_start_p lb, lexeme_end_p lb);
     Format.eprintf "syntax error@.";
     exit 1
  | e -> Format.eprintf "Anomaly: %s\n@." (Printexc.to_string e); exit 2
